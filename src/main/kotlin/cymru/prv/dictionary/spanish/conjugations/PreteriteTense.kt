package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.spanish.SpanishVerb
import cymru.prv.dictionary.spanish.SpanishVerb.ConjugationClass
import org.json.JSONObject

/**
 * @author Preben Vangberg
 */
class PreteriteTense(obj: JSONObject, verb: SpanishVerb): SpanishConjugation(obj, verb) {

    override fun getDefaultSingularFirst(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "é")
            else -> mutableListOf(stem + "í")
        }
    }

    override fun getDefaultSingularSecond(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "aste")
            else -> mutableListOf(stem + "iste")
        }
    }

    override fun getDefaultSingularThird(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "ó")
            else -> mutableListOf(stem + "ió")
        }
    }

    override fun getDefaultPluralFirst(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "amos")
            else -> mutableListOf(stem + "imos")
        }
    }

    override fun getDefaultPluralSecond(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "asteis")
            else -> mutableListOf(stem + "isteis")
        }
    }

    override fun getDefaultPluralThird(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "aron")
            else -> mutableListOf(stem + "ieron")
        }
    }
}