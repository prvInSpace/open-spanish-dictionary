package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.common.Conjugation
import cymru.prv.dictionary.spanish.SpanishVerb
import org.json.JSONObject

/**
 * @author Preben Vangberg
 * @since 1.0.0
 */
abstract class SpanishConjugation(obj: JSONObject, val verb: SpanishVerb): Conjugation(obj) {

    val stem: String = obj.optString("stem", verb.stem)
    val conjugationClass = obj.optString("class", null)
        ?.let { SpanishVerb.ConjugationClass.valueOf(it.uppercase()) }
        ?: verb.conjugationClass

    abstract override fun getDefaultSingularFirst(): MutableList<String>

    abstract override fun getDefaultSingularSecond(): MutableList<String>

    abstract override fun getDefaultSingularThird(): MutableList<String>

    abstract override fun getDefaultPluralFirst(): MutableList<String>

    abstract override fun getDefaultPluralSecond(): MutableList<String>

    abstract override fun getDefaultPluralThird(): MutableList<String>

}