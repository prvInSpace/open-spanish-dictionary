package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.spanish.SpanishVerb
import cymru.prv.dictionary.spanish.SpanishVerb.ConjugationClass
import org.json.JSONObject

/**
 * @author Preben Vangberg
 */
class PresentTense(obj: JSONObject, verb: SpanishVerb): SpanishConjugation(obj, verb) {

    // Alternative stem that is used for singFirst, second, third, and PlurThird if present
    val altStem = obj.optString("altStem", stem)

    override fun getDefaultSingularFirst(): MutableList<String> {
        return mutableListOf(altStem + "o")
    }

    override fun getDefaultSingularSecond(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(altStem + "as")
            else -> mutableListOf(altStem + "es")
        }
    }

    override fun getDefaultSingularThird(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(altStem + "a")
            else -> mutableListOf(altStem + "e")
        }
    }

    override fun getDefaultPluralFirst(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "amos")
            ConjugationClass.ER -> mutableListOf(stem + "emos")
            else -> mutableListOf(stem + "imos")
        }
    }

    override fun getDefaultPluralSecond(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(stem + "áis")
            ConjugationClass.ER -> mutableListOf(stem + "éis")
            else -> mutableListOf(stem + "ís")
        }
    }

    override fun getDefaultPluralThird(): MutableList<String> {
        return when(conjugationClass){
            ConjugationClass.AR -> mutableListOf(altStem + "an")
            else -> mutableListOf(altStem + "en")
        }
    }
}