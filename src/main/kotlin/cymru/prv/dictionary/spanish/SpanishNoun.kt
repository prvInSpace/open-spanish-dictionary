package cymru.prv.dictionary.spanish

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.GrammaticalGender
import cymru.prv.dictionary.common.Word
import cymru.prv.dictionary.common.WordType
import org.json.JSONObject

/**
 * @author Preben Vangberg
 */
class SpanishNoun(obj: JSONObject): Word(obj, WordType.noun)  {

    companion object {
        val FEMININE_SUFFIXES = ".*([cs]ión|[td]ad|ez|a)$".toRegex()
    }

    private val gender = when {
        obj.has("gender") -> GrammaticalGender
            .fromString(obj.getString("gender"))
        normalForm.endsWith("ista") -> setOf(
            GrammaticalGender.MASCULINE,
            GrammaticalGender.FEMININE
        )
        FEMININE_SUFFIXES.matches(normalForm) -> setOf(GrammaticalGender.FEMININE)
        else -> setOf(GrammaticalGender.MASCULINE)
    }

    @Override
    override fun toJson(): JSONObject {
        return super.toJson()
            .put("gender", gender.joinToString(separator = "") { it.toString() })
    }

}