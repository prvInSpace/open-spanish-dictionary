package cymru.prv.dictionary.spanish

import cymru.prv.dictionary.common.DictionaryList
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path


class TestSpanishDictionary {

    @Test
    fun `number of words should be able to load words`(){
        assertTrue(SpanishDictionary(DictionaryList()).numberOfWords > 0)
    }

    @Test
    fun ensureThatVersionNumbersAreTheSame() {
        for (line in Files.readAllLines(Path.of("build.gradle"))) {
            if (line.startsWith("version")) {
                val version = line
                    .split("\\s+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
                    .replace("'", "")
                Assertions.assertEquals(version, SpanishDictionary().version)
                return
            }
        }
        Assertions.fail<Any>("Version number not found in build.gradle")
    }

}