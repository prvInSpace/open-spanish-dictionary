package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.spanish.SpanishVerb
import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

/**
 * @author Preben Vangberg
 */
class TestSpanishConjugationWithShortForm {

    @Test
    fun `words with shortform should drop the 2 first characters`(){
        val word = SpanishVerb(JSONObject()
            .put("normalForm", "querer")
            .put("future", JSONObject()
                .put("stem", "querr")
                .put("shortForm", true)
            )
        )

        val future = word.toJson()
            .getJSONObject("conjugations")
            .getJSONObject("future")
        Assertions.assertEquals("querré", future.getJSONArray("singFirst")[0])
        Assertions.assertEquals("querrás", future.getJSONArray("singSecond")[0])
        Assertions.assertEquals("querrá", future.getJSONArray("singThird")[0])
        Assertions.assertEquals("querremos", future.getJSONArray("plurFirst")[0])
        Assertions.assertEquals("querréis", future.getJSONArray("plurSecond")[0])
        Assertions.assertEquals("querrán", future.getJSONArray("plurThird")[0])
    }


}