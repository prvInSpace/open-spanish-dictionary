package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.spanish.SpanishVerb
import cymru.prv.dictionary.spanish.SpanishVerb.ConjugationClass
import org.json.JSONObject
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test

/**
 * Tests that the present tense is exported correctly
 * and that the verbs are conjugated correctly based on
 * their verb class.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class TestPreteriteTense: TestConjugation() {

    /**
     * Shortened version of testConjugationForm
     * with preterite tense already inserted
     */
    private fun testExpected(
        normalForm: String,
        expectedClass: ConjugationClass,
        expectedForm: String,
        form: String
    ) = testConjugationForm(normalForm, expectedClass, expectedForm, "preterite", form)

    @Test
    fun `should not export if has flag is not set`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "ser")
            .put("preterite", JSONObject()
                .put("has", false)
            )
        )
        assertFalse(verb.conjugations.has("preterite"))
    }

    /* Overrides */
    @Test
    fun `test verb overrides`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "ser")
            .put("present", JSONObject()
                .put("singFirst", "fui")
                .put("singSecond", "fuiste")
                .put("singThird", "fue")
                .put("plurFirst", "fuimos")
                .put("plurSecond", "fuisteis")
                .put("plurThird", "fueron")
            )
        )
        val present = verb.conjugations.getJSONObject("present")
        assertEquals("fui", present.getJSONArray("singFirst")[0])
        assertEquals("fuiste", present.getJSONArray("singSecond")[0])
        assertEquals("fue", present.getJSONArray("singThird")[0])
        assertEquals("fuimos", present.getJSONArray("plurFirst")[0])
        assertEquals("fuisteis", present.getJSONArray("plurSecond")[0])
        assertEquals("fueron", present.getJSONArray("plurThird")[0])
    }

    /* Singular First */
    @Test
    fun `ar-class words should return stem +é for sing first`() =
        testExpected("amar", ConjugationClass.AR, "amé", "singFirst")

    @Test
    fun `er-class words should return stem +í for sing first`() =
        testExpected("temer", ConjugationClass.ER, "temí", "singFirst")

    @Test
    fun `ir-class words should return stem +í for sing first`() =
        testExpected("partir", ConjugationClass.IR, "partí", "singFirst")


    /* Singular Second */
    @Test
    fun `ar-class words should return stem +aste for singular second`() =
        testExpected("amar", ConjugationClass.AR, "amaste", "singSecond")

    @Test
    fun `er-class words should return stem +iste for singular second`() =
        testExpected("temer", ConjugationClass.ER, "temiste", "singSecond")

    @Test
    fun `ir-class words should return stem iste for singular second`() =
        testExpected("partir", ConjugationClass.IR, "partiste", "singSecond")


    /* Singular Third */
    @Test
    fun `ar-class words should return stem +ó for singular third`() =
        testExpected("amar", ConjugationClass.AR, "amó", "singThird")

    @Test
    fun `er-class words should return stem +ió for singular third`() =
        testExpected("temer", ConjugationClass.ER, "temió", "singThird")

    @Test
    fun `ir-class words should return stem +ió for singular third`() =
        testExpected("partir", ConjugationClass.IR, "partió", "singThird")


    /* Plural First */
    @Test
    fun `ar-class words should return stem +amos for plural first`() =
        testExpected("amar", ConjugationClass.AR, "amamos", "plurFirst")

    @Test
    fun `er-class words should return stem +imos for plural first`() =
        testExpected("temer", ConjugationClass.ER, "temimos", "plurFirst")

    @Test
    fun `ir-class words should return stem +imos for plural first`() =
        testExpected("vivir", ConjugationClass.IR, "vivimos", "plurFirst")


    /* Plural Second */
    @Test
    fun `ar-class words should return stem +asteis for plural second`() =
        testExpected("amar", ConjugationClass.AR, "amasteis", "plurSecond")

    @Test
    fun `er-class words should return stem +isteis for plural second`() =
        testExpected("temer", ConjugationClass.ER, "temisteis", "plurSecond")

    @Test
    fun `ir-class words should return stem +isteis for plural second`() =
        testExpected("vivir", ConjugationClass.IR, "vivisteis", "plurSecond")


    /* Plural Third */
    @Test
    fun `ar-class words should return stem +aron for plural third`() =
        testExpected("amar", ConjugationClass.AR, "amaron", "plurThird")

    @Test
    fun `er-class words should return stem +ieron for plural third`() =
        testExpected("temer", ConjugationClass.ER, "temieron", "plurThird")

    @Test
    fun `ir-class words should return stem +en for plural third`() =
        testExpected("vivir", ConjugationClass.IR, "vivieron", "plurThird")
}