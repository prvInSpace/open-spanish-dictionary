package cymru.prv.dictionary.spanish.conjugations

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.spanish.SpanishVerb
import cymru.prv.dictionary.spanish.SpanishVerb.ConjugationClass
import org.json.JSONObject
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test

/**
 * Tests that the imperfect tense is exported correctly
 * and that the verbs are conjugated correctly based on
 * their verb class.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class TestImperfectTense: TestConjugation() {

    /**
     * Shortened version of testConjugationForm
     * with imperfect tense already inserted
     */
    private fun testExpected(
        normalForm: String,
        expectedClass: ConjugationClass,
        expectedForm: String,
        form: String
    ) = testConjugationForm(normalForm, expectedClass, expectedForm, "imperfect", form)

    @Test
    fun `should not export if has flag is not set`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "ser")
            .put("imperfect", JSONObject()
                .put("has", false)
            )
        )
        assertFalse(verb.conjugations.has("imperfect"))
    }

    /* Overrides */
    @Test
    fun `test verb overrides`(){
        val verb = SpanishVerb(JSONObject()
            .put("normalForm", "ser")
            .put("imperfect", JSONObject()
                .put("singFirst", "era")
                .put("singSecond", "eras")
                .put("singThird", "era")
                .put("plurFirst", "éramos")
                .put("plurSecond", "erais")
                .put("plurThird", "eran")
            )
        )
        val present = verb.conjugations.getJSONObject("imperfect")
        assertEquals("era", present.getJSONArray("singFirst")[0])
        assertEquals("eras", present.getJSONArray("singSecond")[0])
        assertEquals("era", present.getJSONArray("singThird")[0])
        assertEquals("éramos", present.getJSONArray("plurFirst")[0])
        assertEquals("erais", present.getJSONArray("plurSecond")[0])
        assertEquals("eran", present.getJSONArray("plurThird")[0])
    }

    /* Singular First */
    @Test
    fun `ar-class words should return stem +aba for sing first`() =
        testExpected("amar", ConjugationClass.AR, "amaba", "singFirst")

    @Test
    fun `er-class words should return stem +ía for sing first`() =
        testExpected("temer", ConjugationClass.ER, "temía", "singFirst")

    @Test
    fun `ir-class words should return stem +ía for sing first`() =
        testExpected("partir", ConjugationClass.IR, "partía", "singFirst")


    /* Singular Second */
    @Test
    fun `ar-class words should return stem +abas for singular second`() =
        testExpected("amar", ConjugationClass.AR, "amabas", "singSecond")

    @Test
    fun `er-class words should return stem +ías for singular second`() =
        testExpected("temer", ConjugationClass.ER, "temías", "singSecond")

    @Test
    fun `ir-class words should return stem +ías for singular second`() =
        testExpected("partir", ConjugationClass.IR, "partías", "singSecond")


    /* Singular Third */
    @Test
    fun `ar-class words should return stem +aba for singular third`() =
        testExpected("amar", ConjugationClass.AR, "amaba", "singThird")

    @Test
    fun `er-class words should return stem +ía for singular third`() =
        testExpected("temer", ConjugationClass.ER, "temía", "singThird")

    @Test
    fun `ir-class words should return stem +ía for singular third`() =
        testExpected("partir", ConjugationClass.IR, "partía", "singThird")


    /* Plural First */
    @Test
    fun `ar-class words should return stem +ábamos for plural first`() =
        testExpected("amar", ConjugationClass.AR, "amábamos", "plurFirst")

    @Test
    fun `er-class words should return stem +íamos for plural first`() =
        testExpected("temer", ConjugationClass.ER, "temíamos", "plurFirst")

    @Test
    fun `ir-class words should return stem +íamos for plural first`() =
        testExpected("vivir", ConjugationClass.IR, "vivíamos", "plurFirst")


    /* Plural Second */
    @Test
    fun `ar-class words should return stem +abais for plural second`() =
        testExpected("amar", ConjugationClass.AR, "amabais", "plurSecond")

    @Test
    fun `er-class words should return stem +íais for plural second`() =
        testExpected("temer", ConjugationClass.ER, "temíais", "plurSecond")

    @Test
    fun `ir-class words should return stem +íais for plural second`() =
        testExpected("vivir", ConjugationClass.IR, "vivíais", "plurSecond")


    /* Plural Third */
    @Test
    fun `ar-class words should return stem +aban for plural third`() =
        testExpected("amar", ConjugationClass.AR, "amaban", "plurThird")

    @Test
    fun `er-class words should return stem +ían for plural third`() =
        testExpected("temer", ConjugationClass.ER, "temían", "plurThird")

    @Test
    fun `ir-class words should return stem +ían for plural third`() =
        testExpected("vivir", ConjugationClass.IR, "vivían", "plurThird")
}