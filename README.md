# Open Spanish Dictionary

[![pipeline status](https://gitlab.com/prvInSpace/open-spanish-dictionary/badges/master/pipeline.svg)](https://gitlab.com/prvInSpace/open-spanish-dictionary/-/commits/master)
[![coverage report](https://gitlab.com/prvInSpace/open-spanish-dictionary/badges/master/coverage.svg)](https://gitlab.com/prvInSpace/open-spanish-dictionary/-/commits/master) 
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![Open Dictionary: v1.2](https://img.shields.io/static/v1?label=Open-Dictionary&message=v1.2&color=informational&link=https://gitlab.com/prvInSpace/open-dictionary-library)](https://gitlab.com/prvInSpace/open-dictionary-library)
[![Discord](https://img.shields.io/discord/718008955040956456.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.gg/UzaFmfV)

The Open Spanish Dictionary is a Spanish part of the [Open Source Dictionary project](https://opensourcedict.org).
It is based on the [Open Dictionary library](https://gitlab.com/prvInSpace/open-dictionary-library).

## Community

Due to the history of the project, most of the developer community is located in the projects [Discord server](https://discord.gg/UzaFmfV). If you got any questions, suggestions, you need any help, or if you just want to pop by to have a cup of coffee or tea, then feel free to join! You are more than welcome to pop by!

The main website for the project is [opensourcedict.org](https://opensourcedict.org).
Here you'll find an interface that allows you to access the different dictionaries, download them as JSON files, and learn more about the project.

## Importing the Library using Gradle

The latest version of the library is hosted through the package repository here on Gitlab.
That means that you are able to import the library using several build tools.

In the list of repositories in your build.gradle file add the following repositories:
```
repositories {
    maven {
        url "https://gitlab.com/api/v4/projects/24450764/packages/maven"
        name "Common Library"
    }
    maven {
        url "https://gitlab.com/api/v4/projects/28904146/packages/maven"
        name "Spanish Dictionary"
    }
}
```
And then in the list of dependencies add a reference to the library like this:
```
dependencies {
    implementation 'cymru.prv:open-spanish-dictionary:1.2.+'
}
```

## Maintainers

* Preben Vangberg &lt;prv21fgt@bangor.ac.uk&gt;

